#-------------------------------------------------
#
# Project created by QtCreator 2015-05-05T10:45:55
#
#-------------------------------------------------

QT       += core widgets opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = surface2
TEMPLATE = app


SOURCES += main.cpp

HEADERS  +=

FORMS    +=

RESOURCES += \
    res.qrc
