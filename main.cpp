#include <QApplication>
#include <QCoreApplication>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>
#include <QOpenGLShader>
#include <QOpenGLTexture>
#include <QLabel>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setMinorVersion( 2 );
    format.setMajorVersion( 4 );
    format.setProfile( QSurfaceFormat::CompatibilityProfile );
//    format.setProfile( QSurfaceFormat::CoreProfile );

    QOpenGLContext context;
    context.setFormat(format);
    if(!context.create()){        
        qFatal("Cannot create the requested OpenGL context!");
    }

    QOffscreenSurface surface;
    surface.setFormat( format );
    surface.create();
    context.makeCurrent( &surface );

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    const float c_01SquareVertices[8] ={0.0f, 0.0f,
                                        1.0f, 0.0f,
                                        1.0f, 1.0f,
                                        0.0f, 1.0f};
    glVertexPointer(2, GL_FLOAT, 0, c_01SquareVertices);
    glTexCoordPointer(2, GL_FLOAT, 0, c_01SquareVertices);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);
    int	maxTextureUnits;
    glGetIntegerv ( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextureUnits );
    qDebug()<<"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS" << maxTextureUnits;

    QImage texImg = QImage(":/tex/tex");
    QOpenGLTexture tex(texImg.mirrored());
    QImage texImg1 = QImage(":/tex/tex1");
    QOpenGLTexture tex1(texImg1.mirrored());
    QImage texImg2 = QImage(":/tex/tex2");
    QOpenGLTexture tex2(texImg2.mirrored());

    QString fsc =
            "uniform sampler2D tex;"
            "uniform sampler2D tex1;"
            "uniform sampler2D tex2;"
            "varying vec4 gl_TexCoord[];"
            "void main(void)"
            "{"
//            "   gl_FragColor = texture2D(tex2, gl_TexCoord[0].yx * 2.0);"
            "   gl_FragColor = texture2D(tex1, gl_TexCoord[0].xy) + texture2D(tex2, gl_TexCoord[0].xy);"
            "}";

    QOpenGLShader fsh( QOpenGLShader::Fragment, &context );
    fsh.compileSourceCode( fsc );

    QOpenGLShaderProgram pr( &context );

    pr.addShader( &fsh );
    pr.link();

    QOpenGLFramebufferObjectFormat fboFormat;
//    fboFormat.setInternalTextureFormat(GL_ALPHA32F);
    QOpenGLFramebufferObject fbo( 1000, 1000, fboFormat );
    fbo.bind();
        glViewport(0,0,fbo.width(),fbo.height());
        glClear(GL_COLOR_BUFFER_BIT);
        pr.bind();

        tex.bind(1);
        pr.setUniformValue("tex", GLuint(1));

        tex1.bind(2);
        pr.setUniformValue("tex1", GLuint(2));

        tex2.bind(3);
        pr.setUniformValue("tex2", GLuint(3));


        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    fbo.release();

    QLabel w;
    w.resize(fbo.size());
    w.setPixmap(QPixmap::fromImage(fbo.toImage()));
    w.show();

    return a.exec();
}
